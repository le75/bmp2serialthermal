import sys
from PIL import Image
from thermalprinter import *

with ThermalPrinter(port='/dev/ttyUSB0', most_heated_point=6) as printer:
    for img in sys.argv[1:]:
        # Picture
        printer.image(Image.open(img))
